package gui;
import buzzword.BuzzWord;
import buzzword.BuzzWordController;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.util.HashMap;

/**
 * Created by Jack on 11/28/2016.
 */
public class GraphNode {

    private StackPane stack;
    private GraphNode top = null;
    private GraphNode left = null;
    private GraphNode right = null;
    private GraphNode bottom = null;
    private static GraphNode prevSelected = null;
    private boolean success = false;
    private boolean selected = false;
    private boolean currentlySelected = false;
    private boolean visited = false;

    private HashMap<Integer, GraphNode> nodeMap = new HashMap<>();


    public GraphNode(StackPane stack){

        this.stack = stack;

    }

    public void connectGraph(){
        int counter = 0;
        while(counter < 16){
            GraphNode node = nodeMap.get(counter);
            if(counter != 3 && counter != 7 && counter != 11 && counter != 15)
                node.setRight(nodeMap.get(counter+1));
            if(counter > 3)
                node.setTop(nodeMap.get(counter-4));
            if(counter < 12)
                node.setBottom(nodeMap.get(counter+4));
            if(counter != 0 && counter != 4 && counter != 8 && counter != 12)
                node.setLeft(nodeMap.get(counter-1));
            counter++;
        }
    }

    public String getText(){
        Text text = (Text)stack.getChildren().get(1);
        return text.getText();
    }

    public void setText(String s){
        ((Text)stack.getChildren().get(1)).setText(s);
    }

    public HashMap getNodeMap(){
        return nodeMap;
    }

    public GraphNode getTop() {
        return top;
    }

    public GraphNode getLeft() {
        return left;
    }

    public GraphNode getRight() {
        return right;
    }

    public GraphNode getBottom() { return bottom; }

    public StackPane getStack(){
        return stack;
    }

    public void setTop(GraphNode g){
        top = g;
    }

    public void setBottom(GraphNode g){
        bottom = g;
    }

    public void setLeft(GraphNode g){
        left = g;
    }

    public void setRight(GraphNode g){
        right = g;
    }

    public void setNodeMap(HashMap<Integer, GraphNode> map){
        nodeMap = map;
    }

    public void setSuccess(boolean b){
        success = b;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setStack(StackPane stack){
        this.stack = stack;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) { this.selected = selected; }


    public boolean isAdjacent() {

        if(getTop() != null && getTop().isCurrentlySelected())
            return true;
        if(getBottom() != null && getBottom().isCurrentlySelected())
            return true;
        if(getLeft() != null && getLeft().isCurrentlySelected())
            return true;
        if(getRight() != null && getRight().isCurrentlySelected())
            return true;

        return false;
    }


    public boolean isCurrentlySelected() {
        return currentlySelected;
    }

    public void setCurrentlySelected(boolean currentlySelected) {
        this.currentlySelected = currentlySelected;

        if(currentlySelected) {
            if (prevSelected != null)
                prevSelected.setCurrentlySelected(false);

            prevSelected = this;
        }
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
