package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import buzzword.BuzzWordController;
import data.GameData;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Pair;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.IOException;
import java.util.*;

import static buzzword.BuzzWordProperties.EXIT_APP_MESSAGE;
import static buzzword.BuzzWordProperties.EXIT_APP_TITLE;
import static gui.WorkspaceProperties.*;

/**
 * @author Jack Smith ID: 110366081
 */

public class Workspace extends AppWorkspaceComponent {

    AppTemplate app;
    AppGUI gui;
    Stage primaryStage;
    BuzzWordController controller;
    GameData gameData;

    Label guiHeadingLabel;
    HBox head;
    BorderPane mainWindow;
    BorderPane gameWindow;

    Button btnCreateProfile;
    Button btnLogin;
    Button btnLogout;

    VBox menuOptions;
    Button btnProfileName;
    Button btnStartPlaying;
    ComboBox modeSelect;

    public GridPane grid;

    VBox levelSelect;
    Label levelHeadingLabel;

    HBox bottomToolbar;
    Button btnHelp;
    Button btnHome;
    Button btnPause;

    VBox gameTextPane;
    VBox guessBox;
    public Label timeRemaining;
    Label guessBoxLabel;
    TableView guessedWords;
    Label bottomLevelLabel;
    public HashMap<Integer, GraphNode> nodeMap = new HashMap<>();

    Button btnReplay;
    public Text scoreText;
    public TextField userInput;
    public int targetScore;
    public final ObservableList<Pair<String, Integer>> tableData = FXCollections.observableArrayList();

    PropertyManager propertyManager = PropertyManager.getManager();

    boolean isLoggedIn = false;
    public String levelChoice = "";
    public int levelNumber = 0;
    public String user = "";
    String pass = "";
    String gameWord = "";

    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        gui.setFileController(app);
        primaryStage = app.getStage();
        controller = (BuzzWordController) gui.getFileController();
        gameData = (GameData) app.getDataComponent();
        reloadWorkspace();
        mainWindow = new BorderPane();
        mainMenuGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    public void mainMenuGUI() {

        reloadWorkspace();
        if(controller.isInPlay)
            controller.timer.cancel();
        head = new HBox();
        head.setAlignment(Pos.CENTER);

        menuOptions = new VBox();
        menuOptions.setAlignment(Pos.TOP_CENTER);
        menuOptions.setSpacing(20);
        menuOptions.setVisible(true);

        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        head.getChildren().addAll(guiHeadingLabel);

        if (isLoggedIn) {

            btnProfileName = new Button(user + " (Settings)");
            btnStartPlaying = new Button(propertyManager.getPropertyValue(START_BUTTON));
            btnLogout = new Button(propertyManager.getPropertyValue(LOGOUT_BUTTON));
            modeSelect = new ComboBox();
            modeSelect.getItems().addAll(
                    "English Dictionary",
                    "Animals",
                    "Food",
                    "Places"
            );
            modeSelect.getSelectionModel().select("English Dictionary"); //default

            menuOptions.getChildren().addAll(btnProfileName, modeSelect, btnStartPlaying, btnLogout);
            btnLogout.setVisible(true);
            btnStartPlaying.setVisible(true);

        } else {

            btnCreateProfile = new Button(propertyManager.getPropertyValue(PROFILE_BUTTON));
            btnLogin = new Button(propertyManager.getPropertyValue(LOGIN_BUTTON));
            menuOptions.getChildren().addAll(btnCreateProfile, btnLogin);

        }

        createGrid(4, 4, "BUZZWORDBUZZWORD"); //main gameplay grid for home screen display
        grid.setVgap(30);
        grid.setHgap(30);

        gameWindow = new BorderPane();
        gameWindow.setCenter(grid);

        bottomToolbar = new HBox();
        btnHelp = new Button("?"); //add tool-tip
        btnHome = new Button("Home");
        btnPause = new Button("Pause");
        btnReplay = new Button("Replay");
        btnPause.setVisible(false);
        btnReplay.setVisible(false);
        bottomToolbar.getChildren().addAll(btnPause, btnReplay, btnHome, btnHelp);
        bottomToolbar.setSpacing(10);
        HBox.setHgrow(bottomToolbar, Priority.ALWAYS);

        mainWindow.setTop(head);
        mainWindow.setLeft(menuOptions);
        mainWindow.setCenter(gameWindow);
        mainWindow.setRight(null);
        mainWindow.setBottom(bottomToolbar);

        mainWindow.setMargin(head, new Insets(0, 70, 0, 0));
        mainWindow.setMargin(menuOptions, new Insets(150, 0, 0, 50)); //top, right, bottom, left (why would they do this???)
        mainWindow.setMargin(gameWindow, new Insets(25, 0, 50, 150));
        mainWindow.setMargin(bottomToolbar, new Insets(50, 50, 0, 900));

        workspace.getChildren().addAll(mainWindow);
        setupHandlers();
        initStyle();
    }

    private void levelSelectGUI(){

        System.out.println("Selecting Level...");
        levelChoice = modeSelect.getSelectionModel().getSelectedItem().toString();

        switch(levelChoice){
            case "Animals":
                gameData.setWordListFile("wordlists/animals.txt");
                gameData.setWordListLength(gameData.ANIMAL_WORDS);
                gameData.createHashSet();
                break;
            case "Places":
                gameData.setWordListFile("wordlists/places.txt");
                gameData.setWordListLength(gameData.PLACES_WORDS);
                gameData.createHashSet();
                break;
            case "Food":
                gameData.setWordListFile("wordlists/food.txt");
                gameData.setWordListLength(gameData.FOOD_WORDS);
                gameData.createHashSet();
                break;
            case "English Dictionary":
                gameData.setWordListFile("wordlists/nouns.txt");
                gameData.setWordListLength(gameData.NOUNS_WORDS);
                gameData.createHashSet();
        }

        levelSelect = new VBox();
        levelSelect.setSpacing(20);

        levelHeadingLabel = new Label(propertyManager.getPropertyValue(LEVEL_SELECT_LABEL) + ": " + levelChoice);
        levelHeadingLabel.setAlignment(Pos.TOP_CENTER);
        levelHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL2));

        createLevelGrid(4, 2, "12345678");
        grid.setHgap(20);
        grid.setVgap(40);

        levelSelect.getChildren().addAll(levelHeadingLabel, grid);

        mainWindow.setMargin(levelSelect, new Insets(50, 0, 50, 150));
        mainWindow.setCenter(levelSelect);
        initStyle();

    }

    public void mainGameGUI(String choice, int id){

        if(controller.timer != null)
            controller.timer.cancel();

        System.out.println("Playing " + choice + " level " + id);
        levelNumber = id;

        createGrid(4, 4, "");
        GraphNode graphNode = new GraphNode(new StackPane());
        graphNode.setNodeMap(nodeMap);
        graphNode.connectGraph();
        generateGridWords();
        fillEmptyNodes();
        grid.getChildren().clear();
        addFilledNodes();
        controller.solveGrid();

        grid.setVgap(30);
        grid.setHgap(30);
        gameWindow.setCenter(grid);

        gameTextPane = new VBox();
        gameTextPane.setSpacing(20);

        timeRemaining = new Label("Time Remaining: 60 seconds");
        timeRemaining.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        controller.play();

        guessBox = new VBox();
        guessBox.setSpacing(10);
        guessBoxLabel = new Label("Words Guessed: ");
        setTable();
        guessBox.getChildren().addAll(guessBoxLabel, guessedWords);

        Rectangle rect1 = new Rectangle(200, 100, Paint.valueOf("337AFF"));

        targetScore = controller.generateTargetScore(levelChoice, levelNumber);
        scoreText = new Text("Current Score: 0 \n \n" + "Target Score: " + targetScore + " Points");
        scoreText.setStroke(Paint.valueOf("FFFFFF"));
        StackPane targetScoreBox = new StackPane();
        targetScoreBox.getChildren().addAll(rect1, scoreText);

        userInput = new TextField();
        userInput.setDisable(true);
        userInput.setStyle("-fx-opacity: 1.0; -fx-font-size: 16");
        userInput.clear();

        gameTextPane.getChildren().addAll(timeRemaining, guessBox, userInput, targetScoreBox);
        bottomLevelLabel = new Label(choice + " Level " + id);
        bottomLevelLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL2));

        btnPause.setVisible(true);
        btnReplay.setVisible(true);
        menuOptions.setVisible(false);

        mainWindow.setRight(gameTextPane);
        mainWindow.setMargin(gameTextPane, new Insets(0, 50, 0, 0));
        gameWindow.setBottom(bottomLevelLabel);
        gameWindow.setMargin(bottomLevelLabel, new Insets(0, 0, 0, 25));

        mainWindow.setCenter(gameWindow);
        mainWindow.setMargin(gameWindow, new Insets(50, 50, 0, 150));
        initStyle();

    }

    private void setupHandlers() {

        btnCreateProfile.setOnMouseClicked(e -> createProfile());
        btnLogin.setOnMouseClicked(e -> setLogin());
        btnHome.setOnMouseClicked(e -> mainMenuGUI());
        btnPause.setOnMouseClicked(e -> pause());
        btnReplay.setOnMouseClicked(e -> mainGameGUI(levelChoice, levelNumber));
        btnHelp.setOnMouseClicked(e -> help());
        if(isLoggedIn) {
            btnProfileName.setOnMouseClicked(e -> editProfile());
            btnStartPlaying.setOnMouseClicked(e -> levelSelectGUI());
            btnLogout.setOnMouseClicked(e -> logout());
        }

        //gui.getPrimaryScene().addEventFilter(MouseEvent.ANY, e -> System.out.println( e)); //for mouse debugging

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            YesNoCancelDialogSingleton yesNoDialog = YesNoCancelDialogSingleton.getSingleton();
            @Override
            public void handle(WindowEvent event) {
                event.consume();
                yesNoDialog.show(propertyManager.getPropertyValue(EXIT_APP_TITLE), propertyManager.getPropertyValue(EXIT_APP_MESSAGE));
                if(yesNoDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
                    System.exit(1);

            }
        });

        app.getGUI().getPrimaryScene().setOnKeyPressed((KeyEvent event) -> {
            final KeyCombination keyCtrlH = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN);
            final KeyCombination keyCtrlQ = new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN);
            if(event.getCode() == KeyCode.HOME){
                mainMenuGUI();
            }
            if (keyCtrlH.match(event)) {
                help();
            }
            if(keyCtrlQ.match(event)){
                quit();
            }

        });

    }

    public void highlightOnMouseOver(StackPane stack){
        DropShadow unselected = new DropShadow();
        unselected.setOffsetY(4);
        DropShadow selected = new DropShadow();
        selected.setColor(Color.BLUEVIOLET);
        selected.setRadius(20);
        stack.getChildren().get(0).setEffect(unselected);
        stack.setOnMouseEntered(e -> stack.getChildren().get(0).setEffect(selected));
        stack.setOnMouseExited(e -> stack.getChildren().get(0).setEffect(unselected));

    }

    private void gameStackHandler(GraphNode node){
        node.getStack().setOnMouseClicked(e -> controller.handleWordSelected(node));
    }


    private void createGrid(int row, int col, String s) {

        grid = new GridPane();
        int counter = 0;
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                StackPane stack = new StackPane();
                Text letter = new Text("");
                if(s.length() == 16) {
                    letter = new Text("" + s.charAt(counter));
                }
                letter.setFill(Paint.valueOf("FFFFFF"));
                letter.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
                stack.getChildren().addAll(constructCircle(40, "337AFF"), letter);
                highlightOnMouseOver(stack);
                GraphNode node = new GraphNode(stack);
                nodeMap.put(counter, node);
                if(s.length() == 0)
                    gameStackHandler(nodeMap.get(counter));
                grid.add(stack, j, i);
                counter++;
            }
        }

    }

    private void generateGridWords(){

        String s = "";
        while(s.length() < 16){
            gameWord = controller.getRandomWord();
            s+=gameWord;
            if(s.length() > 13)
                return;
            populateGrid();
        }

    }

    private void fillEmptyNodes(){

        StackPane stack;
        for(int i = 0; i < 16; i++){
            if(nodeMap.get(i).getText().equals("")) {
                int rand = new Random().nextInt(26)+65;
                stack = nodeMap.get(i).getStack();
                ((Text) stack.getChildren().get(1)).setText((char)rand + "");
            }
        }
    }

    private void addFilledNodes(){
        int counter = 0;
        StackPane stack;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                stack = nodeMap.get(counter).getStack();
                grid.add(stack, j, i);
                counter++;
            }
        }
    }

    private void populateGrid() {

        int start = new Random().nextInt(15);
        GraphNode startNode = nodeMap.get(start);
        clearGridText();
        getNextStack(startNode, 0);
        for(int i = 0; i < 16; i++){
            if(!nodeMap.get(i).getText().equals(""))
                nodeMap.get(i).setSuccess(true); //marks nodes containing letters from words that do not get stuck, so they are not removed when new words are added
        }

    }

    private void clearGridText(){
        for(int i = 0; i < 16; i++){
            if(!nodeMap.get(i).getSuccess())
                nodeMap.get(i).setText("");
        }
    }

    private void getNextStack(GraphNode node, int counter) {

        if (counter > gameWord.length() - 1)
            return;

        StackPane stack;
        String letter = Character.toUpperCase(gameWord.charAt(counter)) + "";
        ArrayList<Integer> list = checkAvailableNodes(node, letter);


        if (list.isEmpty()) { //we are stuck at this point, retry
            populateGrid();
            if(list.size() == 0) //ensure that 0 is never passed to nextInt() or else code will break
                return;
        }

        int rand = new Random().nextInt(list.size());

        switch (list.get(rand)) {
            case 0: //left
                stack = node.getLeft().getStack();
                ((Text)stack.getChildren().get(1)).setText(letter+"");
                node = node.getLeft();
                getNextStack(node, counter + 1);
                break;
            case 1: //right
                stack = node.getRight().getStack();
                ((Text)stack.getChildren().get(1)).setText(letter+"");
                node = node.getRight();
                getNextStack(node, counter + 1);
                break;
            case 2: //top
                stack = node.getTop().getStack();
                ((Text)stack.getChildren().get(1)).setText(letter+"");
                node = node.getTop();
                getNextStack(node, counter + 1);
                break;
            case 3: //bottom
                stack = node.getBottom().getStack();
                ((Text)stack.getChildren().get(1)).setText(letter+"");
                node = node.getBottom();
                getNextStack(node, counter + 1);
                break;
        }
    }

    private ArrayList<Integer> checkAvailableNodes(GraphNode node, String letter) {

        ArrayList<Integer> list = new ArrayList<>();

        if (node.getLeft() != null && (node.getLeft().getText().equals("") || (node.getLeft().getText().equals(letter) && node.getLeft().getSuccess())))
            list.add(0);
        if (node.getRight() != null && (node.getRight().getText().equals("") || (node.getRight().getText().equals(letter) && node.getRight().getSuccess())))
            list.add(1);
        if (node.getTop() != null && (node.getTop().getText().equals("") || (node.getTop().getText().equals(letter) && node.getTop().getSuccess())))
            list.add(2);
        if (node.getBottom() != null && (node.getBottom().getText().equals("") || (node.getBottom().getText().equals(letter) && node.getBottom().getSuccess())))
            list.add(3);

        return list; //if no spots available, it is stuck

    }

    public void updateGrid(){
        for(int i = 0; i < 16; i++){
            GraphNode node = nodeMap.get(i);
            if(!node.isSelected()){
                if(node.getLeft() != null && !node.getLeft().isSelected()){
                    Circle circ = (Circle) node.getStack().getChildren().get(0);
                    circ.setFill(Paint.valueOf("337AFF"));
                    node.getStack().setOnMouseEntered(null);
                    node.getStack().setOnMouseClicked(e -> controller.handleWordSelected(node));
                }
                if(node.getRight() != null && !node.getRight().isSelected()){
                    Circle circ = (Circle) node.getStack().getChildren().get(0);
                    circ.setFill(Paint.valueOf("337AFF"));
                    node.getStack().setOnMouseEntered(null);
                    node.getStack().setOnMouseClicked(e -> controller.handleWordSelected(node));
                }
                if(node.getTop() != null && !node.getTop().isSelected()){
                    Circle circ = (Circle) node.getStack().getChildren().get(0);
                    circ.setFill(Paint.valueOf("337AFF"));
                    node.getStack().setOnMouseEntered(null);
                    node.getStack().setOnMouseClicked(e -> controller.handleWordSelected(node));
                }
                if(node.getBottom() != null && !node.getBottom().isSelected()){
                    Circle circ = (Circle) node.getStack().getChildren().get(0);
                    circ.setFill(Paint.valueOf("337AFF"));
                    node.getStack().setOnMouseEntered(null);
                    node.getStack().setOnMouseClicked(e -> controller.handleWordSelected(node));
                }

            }
        }
    }

    public void resetGridHighlight(){
        GraphNode node;
        for(int i = 0; i < 16; i++){
            node = nodeMap.get(i);
            node.setSelected(false);
        }

        updateGrid();

        for(int j = 0; j < 16; j++){
            highlightOnMouseOver(nodeMap.get(j).getStack());
        }
    }

    private void createLevelGrid(int row, int col, String s){
        grid = new GridPane();
        int counter = 0;
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                int id = counter+1;
                StackPane stack = new StackPane();
                Text letter = new Text(""+ s.charAt(counter));
                letter.setFont(Font.font("Verdana", 20));
                if(counter <= gameData.getLevelsCleared(modeSelect.getSelectionModel().getSelectedIndex())) {
                    letter.setStroke(Paint.valueOf("000000"));
                    stack.getChildren().addAll(constructCircle(50, "FFFFFF"), letter);
                    stack.setOnMouseClicked(e -> mainGameGUI(levelChoice, id));
                    highlightOnMouseOver(stack);
                }else{
                    letter.setStroke(Paint.valueOf("FFFFFF"));
                    stack.getChildren().addAll(constructCircle(50, "000000"), letter);
                }
                grid.add(stack, j, i);
                counter++;
            }
        }

    }

    private Circle constructCircle(double radius, String paintVal){

        Circle circ = new Circle(0, 0, radius, Paint.valueOf(paintVal));
        circ.setStroke(Paint.valueOf("#000000"));
        circ.setStrokeWidth(2);
        return circ;

    }

    private void setTable(){

        guessedWords = new TableView();
        tableData.clear();
        TableColumn words = new TableColumn("Words");
        words.setPrefWidth(160);
        TableColumn pointsAwarded = new TableColumn("Points");
        pointsAwarded.setPrefWidth(160);
        words.setCellValueFactory(
                new PropertyValueFactory<Pair<String, Integer>, String>("key")
        );
        pointsAwarded.setCellValueFactory(
                new PropertyValueFactory<Pair<String, Integer>, Integer>("value")
        );
        guessedWords.setPrefHeight(300);
        guessedWords.getColumns().addAll(words, pointsAwarded);

    }

    public void addToTable(){
        guessedWords.setItems(tableData);
    }

    private void setLogin() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        Pair<String, String> userPass = showUserPassDialog("Login", "Login");
        user = userPass.getKey();
        pass = userPass.getValue();
        if(!user.equals("") || !pass.equals("")) {
            try {
                controller.handleLoadRequest();
            }catch (IOException e){
                dialog.show(propertyManager.getPropertyValue(FILE_NOT_FOUND_TITLE), propertyManager.getPropertyValue(FILE_NOT_FOUND_MESSAGE));
                return;
            }

            if(gameData.md5hash(pass).equals(gameData.getSecurePassword())) {
                dialog.show(propertyManager.getPropertyValue(LOGIN_SUCCESSFUL_TITLE), propertyManager.getPropertyValue(LOGIN_SUCCESSFUL_MESSAGE));
                isLoggedIn = true;
                gameData.setPassword(pass);
                mainMenuGUI();
            }else{
                dialog.show(propertyManager.getPropertyValue(LOGIN_UNSUCCESSFUL_TITLE), propertyManager.getPropertyValue(LOGIN_UNSUCCESSFUL_MESSAGE));
            }
        }

    }

    private void logout(){
        isLoggedIn = false;
        mainMenuGUI();
    }

    private void createProfile(){

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        Pair<String, String> userPass = showUserPassDialog("Create Profile", "Ok");

        if(!userPass.getKey().equals("") && !userPass.getValue().equals("")) {
            gameData.setUsername(userPass.getKey());
            gameData.setPassword(userPass.getValue());
            try {
                controller.handleSaveRequest();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }else{
            dialog.show(propertyManager.getPropertyValue(PROFILE_CREATION_ERROR_TITLE), propertyManager.getPropertyValue(PROFILE_CREATION_ERROR_MESSAGE));
        }
    }

    private void editProfile(){

        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Change Password");
        ButtonType btnOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(btnOK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        PasswordField password = new PasswordField();

        grid.add(new Label("New Password:"), 0, 0);
        grid.add(password, 1, 0);

        Node loginButton = dialog.getDialogPane().lookupButton(btnOK);
        loginButton.setDisable(true);

        password.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(() -> password.requestFocus());
        dialog.showAndWait();

        if(!password.getText().isEmpty()){
            gameData.setPassword(password.getText());
        }

        try {
            app.getFileComponent().saveData(app.getDataComponent(), controller.dataFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Pair<String, String> showUserPassDialog(String title, String button){

        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(title);
        ButtonType btnOK = new ButtonType(button, ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(btnOK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);

        Node loginButton = dialog.getDialogPane().lookupButton(btnOK);
        loginButton.setDisable(true);

        username.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(() -> username.requestFocus());
        dialog.showAndWait();

        return new Pair<>(username.getText(), password.getText());
    }

    public void help(){

        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Help Menu");
        ButtonType btnOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(btnOK);

        ScrollPane sp = new ScrollPane();
        Image help = new Image("/images/help.jpg");

        sp.setContent(new ImageView(help));
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);


        dialog.getDialogPane().setContent(sp);
        dialog.getDialogPane().setPrefHeight(500);
        dialog.showAndWait();

    }

    public void quit(){
        YesNoCancelDialogSingleton yesNoDialog = YesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(propertyManager.getPropertyValue(EXIT_APP_TITLE), propertyManager.getPropertyValue(EXIT_APP_MESSAGE));
        if(yesNoDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            primaryStage.close();
    }


    private void pause(){
        if(btnPause.getText().equals("Pause")) {
            btnPause.setText("Play");
            grid.setVisible(false);
            btnReplay.setDisable(true);
        } else {
            btnPause.setText("Pause");
            grid.setVisible(true);
            btnReplay.setDisable(false);
        }
    }

    @Override
    public void initStyle() {
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
        btnHelp.getStyleClass().setAll(propertyManager.getPropertyValue(HELP_BUTTON));
        btnHome.getStyleClass().setAll(propertyManager.getPropertyValue(HOME_BUTTON));
        btnPause.getStyleClass().setAll(propertyManager.getPropertyValue(HOME_BUTTON));
        btnReplay.getStyleClass().setAll(propertyManager.getPropertyValue(HOME_BUTTON));
        //app.getGUI().getAppPane().getStyleClass().setAll(propertyManager.getPropertyValue(BACKGROUND));

    }

    @Override
    public void reloadWorkspace() {

        workspace = new VBox();
        VBox.setVgrow(workspace, Priority.ALWAYS);
        app.getGUI().getAppPane().setTop(workspace);

    }
}
