package buzzword;

import apptemplate.AppTemplate;
import data.GameData;
import gui.GraphNode;
import gui.Workspace;
import controller.FileController;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.util.Pair;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static buzzword.BuzzWordProperties.*;
import static gui.WorkspaceProperties.PROFILE_CREATED_MESSAGE;
import static gui.WorkspaceProperties.PROFILE_CREATED_TITLE;

/**
 * @author Jack Smith ID: 110366081
 */
public class BuzzWordController implements FileController {

    private AppTemplate appTemplate;
    private GameData gameData;
    public boolean isInPlay = false;
    public Timer timer;
    public Set<String> possibleWords;
    public File dataFile;

    private String inputWord = "";
    private int totalScore = 0;
    private int i = 60;
    private PropertyManager propertyManager = PropertyManager.getManager();

    public BuzzWordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    public String getRandomWord() {
        gameData = (GameData) appTemplate.getDataComponent();
        URL wordsResource = getClass().getClassLoader().getResource(gameData.getWordListFile());
        assert wordsResource != null;

        int toSkip = new Random().nextInt(gameData.getWordListLength());
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            if (word.length() > 16 || word.length() < 3)
                return getRandomWord();
            else
                return word;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return "error";
    }


    public void play() {
        isInPlay = true;
        i = 60;
        totalScore = 0;
        inputWord = "";
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                            if (Character.isAlphabetic(event.getCharacter().charAt(0))) {
                                String letter = Character.toUpperCase(event.getCharacter().charAt(0)) + "";
                                inputWord += letter;
                                gameWorkspace.userInput.setText(inputWord);
                                handleWordTyped();
                            }

                        });

                        appTemplate.getGUI().getPrimaryScene().setOnKeyPressed((KeyEvent event) -> {

                            final KeyCombination keyCtrlH = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN);
                            final KeyCombination keyCtrlQ = new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN);

                            if (event.getCode() == KeyCode.ENTER) {
                                checkSelectedWord();
                            }

                            if (event.getCode() == KeyCode.BACK_SPACE) {
                                if (inputWord.length() > 0) {
                                    inputWord = inputWord.substring(0, inputWord.length() - 1);
                                    gameWorkspace.userInput.setText(inputWord);
                                    handleWordTyped();
                                }
                            }

                            if(event.getCode() == KeyCode.HOME){
                                gameWorkspace.mainMenuGUI();
                            }

                            if (keyCtrlH.match(event)) {
                                gameWorkspace.help();
                            }

                            if(keyCtrlQ.match(event)){
                                gameWorkspace.quit();
                            }


                        });

                        if (gameWorkspace.grid.isVisible())
                            gameWorkspace.timeRemaining.setText("Time Remaining: " + Integer.toString(--i) + " seconds");
                        if (i <= 15)
                            gameWorkspace.timeRemaining.setTextFill(Paint.valueOf("#ff0000"));
                        if (i == 0)
                            stop();
                    }
                });
            }

            void stop() {
                timer.cancel();
                end();
            }

        }, 0, 1000);
    }

    public void end() {
        System.out.println("Ending...");
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.resetGridHighlight();
        gameWorkspace.userInput.clear();
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        GraphNode node;
        for (int i = 0; i < 16; i++) {
            node = gameWorkspace.nodeMap.get(i);
            node.getStack().setOnMouseClicked(null);
            gameWorkspace.highlightOnMouseOver(node.getStack());
        }

        System.out.println("Total Score: " + totalScore);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        YesNoCancelDialogSingleton option = YesNoCancelDialogSingleton.getSingleton();
        if (totalScore >= gameWorkspace.targetScore) {

            if(gameWorkspace.levelNumber < 7) {

                option.show(propertyManager.getPropertyValue(LEVEL_COMPLETED_TITLE), propertyManager.getPropertyValue(LEVEL_COMPLETED_MESSAGE));
                if (gameWorkspace.levelChoice.equals("English Dictionary") && gameData.getLevelsCleared(0) < gameWorkspace.levelNumber)
                    gameData.incrementLevelsCleared(0);
                else if (gameWorkspace.levelChoice.equals("Animals") && gameData.getLevelsCleared(1) < gameWorkspace.levelNumber)
                    gameData.incrementLevelsCleared(1);
                else if (gameWorkspace.levelChoice.equals("Food") && gameData.getLevelsCleared(2) < gameWorkspace.levelNumber)
                    gameData.incrementLevelsCleared(2);
                else if (gameWorkspace.levelChoice.equals("Places") && gameData.getLevelsCleared(3) < gameWorkspace.levelNumber && gameData.getLevelsCleared(3) < 7)
                    gameData.incrementLevelsCleared(3);

                dialog.show(propertyManager.getPropertyValue(GRID_SOLUTION_TITLE), propertyManager.getPropertyValue(GRID_SOLUTION_MESSAGE) + possibleWords);

                if (option.getSelection().equals(YesNoCancelDialogSingleton.YES))
                    gameWorkspace.mainGameGUI(gameWorkspace.levelChoice, ++gameWorkspace.levelNumber);
            }else{

                dialog.show(propertyManager.getPropertyValue(FINAL_LEVEL_COMPLETED_TITLE), propertyManager.getPropertyValue(FINAL_LEVEL_COMPLETED_MESSAGE));
                dialog.show(propertyManager.getPropertyValue(GRID_SOLUTION_TITLE), propertyManager.getPropertyValue(GRID_SOLUTION_MESSAGE) + possibleWords);
            }

        } else {

            option.show(propertyManager.getPropertyValue(LEVEL_INCOMPLETE_TITLE), propertyManager.getPropertyValue(LEVEL_INCOMPLETE_MESSAGE));
            dialog.show(propertyManager.getPropertyValue(GRID_SOLUTION_TITLE), propertyManager.getPropertyValue(GRID_SOLUTION_MESSAGE) + possibleWords);

            if (option.getSelection().equals(YesNoCancelDialogSingleton.YES))
                gameWorkspace.mainGameGUI(gameWorkspace.levelChoice, gameWorkspace.levelNumber);

        }


        if (totalScore > gameData.getHighScore()) {
            dialog.show(propertyManager.getPropertyValue(HIGH_SCORE_TITLE), propertyManager.getPropertyValue(HIGH_SCORE_MESSAGE) + totalScore);
            gameData.setHighScore(totalScore);
        }

        try {
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), dataFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handleWordSelected(GraphNode node) {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        highlightStack(node, false);
        gameWorkspace.updateGrid();
        highlightAdjacentStacks(node);

    }

    public void handleWordTyped() {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        GraphNode node;

        if (inputWord.length() == 0)
            gameWorkspace.resetGridHighlight();

        if (inputWord.length() == 1) {
            for (int i = 0; i < 16; i++) {
                node = gameWorkspace.nodeMap.get(i);
                if (node.getText().equals(inputWord)) {
                    Circle circ = (Circle) node.getStack().getChildren().get(0);
                    circ.setFill(Paint.valueOf("#7CFC00"));
                    node.setSelected(true);
                    highlightTypedAdjacentNodes(node);
                }
            }
        }

        if (inputWord.length() > 1) {
            for (int i = 0; i < 16; i++) {
                for (int j = 1; j < inputWord.length(); j++) {
                    node = gameWorkspace.nodeMap.get(i);
                    if (node.isSelected()) {
                        if (node.getTop() != null && node.getTop().getText().equals(inputWord.charAt(j) + "")) {
                            Circle circ = (Circle) node.getTop().getStack().getChildren().get(0);
                            circ.setFill(Paint.valueOf("#7CFC00"));
                            node.getTop().setSelected(true);
                            node.getTop().setCurrentlySelected(true);
                            gameWorkspace.updateGrid();
                            highlightTypedAdjacentNodes(node.getTop());
                        }
                        if (node.getBottom() != null && node.getBottom().getText().equals(inputWord.charAt(j) + "")) {
                            Circle circ = (Circle) node.getBottom().getStack().getChildren().get(0);
                            circ.setFill(Paint.valueOf("#7CFC00"));
                            node.getBottom().setSelected(true);
                            gameWorkspace.updateGrid();
                            highlightTypedAdjacentNodes(node.getBottom());
                        }
                        if (node.getLeft() != null && node.getLeft().getText().equals(inputWord.charAt(j) + "")) {
                            Circle circ = (Circle) node.getLeft().getStack().getChildren().get(0);
                            circ.setFill(Paint.valueOf("#7CFC00"));
                            node.getLeft().setSelected(true);
                            gameWorkspace.updateGrid();
                            highlightTypedAdjacentNodes(node.getLeft());
                        }
                        if (node.getRight() != null && node.getRight().getText().equals(inputWord.charAt(j) + "")) {
                            Circle circ = (Circle) node.getRight().getStack().getChildren().get(0);
                            circ.setFill(Paint.valueOf("#7CFC00"));
                            node.getRight().setSelected(true);
                            gameWorkspace.updateGrid();
                            highlightTypedAdjacentNodes(node.getRight());
                        }
                    }
                }
            }
        }

    }

    private void highlightStack(GraphNode node, boolean isAdjacent) {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        if (!isAdjacent) {
            Circle circ = (Circle) node.getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#7CFC00"));
            node.setSelected(true);
            node.getStack().setOnMouseEntered(null);
            inputWord += node.getText();
            gameWorkspace.userInput.setText(inputWord);
            node.getStack().setOnMouseClicked(e -> checkSelectedWord());
        } else {
            Circle circ = (Circle) node.getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#78AB46"));
            node.getStack().setOnMouseClicked(e -> checkSelectedWord());
            node.getStack().setOnMouseEntered(e -> handleWordSelected(node));
        }

    }

    private void checkSelectedWord() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.resetGridHighlight();
        inputWord = inputWord.toLowerCase();
        int wordScore = inputWord.length() * 3;
        Pair attempt = new Pair<>(inputWord, wordScore);
        if (possibleWords.contains(inputWord) && inputWord.length() > 2 && !gameWorkspace.tableData.contains(attempt)) {
            gameWorkspace.tableData.add(attempt);
            gameWorkspace.addToTable();
            totalScore += wordScore;
            gameWorkspace.scoreText.setText("Current Score: " + totalScore + " Points \n \n" + "Target Score: " + gameWorkspace.targetScore + " Points");
        }
        inputWord = "";
        gameWorkspace.userInput.clear();
    }

    private void highlightAdjacentStacks(GraphNode node) {
        if (node.getTop() != null && !node.getTop().isSelected())
            highlightStack(node.getTop(), true);
        if (node.getBottom() != null && !node.getBottom().isSelected())
            highlightStack(node.getBottom(), true);
        if (node.getLeft() != null && !node.getLeft().isSelected())
            highlightStack(node.getLeft(), true);
        if (node.getRight() != null && !node.getRight().isSelected())
            highlightStack(node.getRight(), true);
    }

    private void highlightTypedAdjacentNodes(GraphNode node) {
        if (node.getTop() != null && !node.getTop().isSelected()) {
            Circle circ = (Circle) node.getTop().getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#78AB46"));
        }
        if (node.getBottom() != null && !node.getBottom().isSelected()) {
            Circle circ = (Circle) node.getBottom().getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#78AB46"));
        }
        if (node.getLeft() != null && !node.getLeft().isSelected()) {
            Circle circ = (Circle) node.getLeft().getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#78AB46"));
        }
        if (node.getRight() != null && !node.getRight().isSelected()) {
            Circle circ = (Circle) node.getRight().getStack().getChildren().get(0);
            circ.setFill(Paint.valueOf("#78AB46"));
        }
    }

    public void solveGrid() {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        possibleWords = new HashSet<>();
        for (String s : gameData.getWordList()) {
            if (s.length() > 2 && s.length() < 14) {
                for (int i = 0; i < 16; i++) {
                    if (gameWorkspace.nodeMap.get(i).getText().equalsIgnoreCase(s.charAt(0) + "")) {
                        if (findAllSequences(gameWorkspace.nodeMap.get(i), s, 1)) {
                            possibleWords.add(s.toLowerCase());
                            clearVisited();
                        }else{
                            clearVisited();
                        }
                    }
                }
            }
        }

        System.out.println("Possible solutions to grid: " + possibleWords);

    }


    private boolean findAllSequences(GraphNode node, String s, int i) {

        if (i == s.length())
            return true;

        node.setVisited(true);

        if (node.getLeft() != null && node.getLeft().getText().equalsIgnoreCase(s.charAt(i) + "") && !node.getLeft().isVisited())
            if (findAllSequences(node.getLeft(), s, i + 1))
                return true;
        if (node.getRight() != null && node.getRight().getText().equalsIgnoreCase(s.charAt(i) + "") && !node.getRight().isVisited())
            if (findAllSequences(node.getRight(), s, i + 1))
                return true;
        if (node.getTop() != null && node.getTop().getText().equalsIgnoreCase(s.charAt(i) + "") && !node.getTop().isVisited())
            if (findAllSequences(node.getTop(), s, i + 1))
                return true;
        if (node.getBottom() != null && node.getBottom().getText().equalsIgnoreCase(s.charAt(i) + "") && !node.getBottom().isVisited())
            if (findAllSequences(node.getBottom(), s, i + 1))
                return true;


        return false;

    }

    private void clearVisited() {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        for (int i = 0; i < 16; i++) {

            gameWorkspace.nodeMap.get(i).setVisited(false);

        }

    }

    public int generateTargetScore(String levelChoice, int levelNumber){

        if(levelChoice.equals("English Dictionary")) {
            return possibleWords.size() * 2 - ((8 - levelNumber) * 2);
        }else{
            String word = "";
            for (String s : possibleWords) {
                if (word.length() < s.length())
                    word = s;
            }

            return word.length() * 3;
        }
    }


    @Override
    public void handleSaveRequest() throws IOException {

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        gameData = (GameData) appTemplate.getDataComponent();
        dataFile = new File("./BuzzWord/saved/" + gameData.getUsername() + ".json");
        if (dataFile.exists()) {
            dialog.show(propertyManager.getPropertyValue(PROFILE_EXISTS_TITLE), propertyManager.getPropertyValue(PROFILE_EXISTS_MESSAGE));
            return;
        }
        dataFile.createNewFile();
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), dataFile.toPath());
        dialog.show(propertyManager.getPropertyValue(PROFILE_CREATED_TITLE), propertyManager.getPropertyValue(PROFILE_CREATED_MESSAGE));

    }

    @Override
    public void handleLoadRequest() throws IOException {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Path toFile = Paths.get("./BuzzWord/saved/" + gameWorkspace.user + ".json");
        dataFile = new File("./BuzzWord/saved/" + gameWorkspace.user + ".json");
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), toFile);

    }


    @Override
    public void handleNewRequest() {

    }

    @Override
    public void handleExitRequest() {

    }
}
