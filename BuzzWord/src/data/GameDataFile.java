package data;

import com.fasterxml.jackson.core.*;
import components.AppFileComponent;
import components.AppDataComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * @author Jack Smith ID: 110366081
 */

public class GameDataFile implements AppFileComponent {

    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String LEVELS_CLEARED = "LEVELS_CLEARED";
    public static final String HIGH_SCORE = "HIGH_SCORE";

    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException {

        GameData gameData = (GameData) data;
        JsonFactory jsonFactory = new JsonFactory();

        try(OutputStream out = Files.newOutputStream(filePath)){

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
            generator.writeStartObject();

            generator.writeStringField(USERNAME, gameData.getUsername());

            generator.writeStringField(PASSWORD, gameData.md5hash(gameData.getPassword()));

            generator.writeFieldName(LEVELS_CLEARED);
            generator.writeArray(gameData.getLevelsClearedArray(), 0, 4);

            generator.writeFieldName(HIGH_SCORE);
            generator.writeNumber(gameData.getHighScore());

            generator.writeEndObject();

            generator.close();

        }catch (IOException e){
            e.printStackTrace();
            System.exit(1);
        }

    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {
        GameData gameData = (GameData) data;

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser = jsonFactory.createParser(Files.newInputStream(filePath));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case USERNAME:
                        jsonParser.nextToken();
                        gameData.setUsername(jsonParser.getValueAsString());
                        break;
                    case PASSWORD:
                        jsonParser.nextToken();
                        gameData.setSecurePassword(jsonParser.getValueAsString());
                        break;
                    case LEVELS_CLEARED:
                        jsonParser.nextToken();
                        int counter = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                            gameData.setLevelsCleared(counter, jsonParser.getIntValue());
                            counter++;
                        }
                        break;
                    case HIGH_SCORE:
                        jsonParser.nextToken();
                        gameData.setHighScore(jsonParser.getValueAsInt());
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON tableData");
                }
            }
        }
    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}
