package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.stream.Stream;

/**
 * @author Jack Smith ID: 110366081
 */

public class GameData implements AppDataComponent{

    public  AppTemplate appTemplate;
    private String username;
    private String password;
    private String securePassword;
    private int[] levelsCleared = {0, 0, 0, 0};
    private HashSet<String> wordList = new HashSet<>();
    private int highScore = 0;

    public String wordListFile;
    public int wordListLength;
    public static final int ANIMAL_WORDS = 224;
    public static final int PLACES_WORDS = 230;
    public static final int FOOD_WORDS = 145;
    public static final int NOUNS_WORDS = 401;


    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if(initiateGame) {
            this.appTemplate = appTemplate;
            //reinitialize new tableData variables
        }else
            this.appTemplate = appTemplate;
    }


    public void createHashSet(){

        String file;
        if(wordListFile.equals("wordlists/nouns.txt")) {
            file ="wordlists/dictionary.txt";
        }else{
            file = wordListFile;
        }

        URL wordsResource = getClass().getClassLoader().getResource(file);
        assert wordsResource != null;


        if(!wordList.isEmpty())
            wordList.clear();

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
                for( String line : (Iterable<String>) lines::iterator ) {
                    wordList.add(line);
                }
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
        }

    }

    public HashSet<String> getWordList() {
        return wordList;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String s){
        username = s;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String s){
        password = s;
    }

    public String md5hash(String pass){
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(pass.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < byteData.length; i++){
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();

        }catch(Exception e){
            e.printStackTrace();
        }
        return "error";
    }

    public String getSecurePassword(){
        return securePassword;
    }

    public void setSecurePassword(String s){
        securePassword = s;
    }

    public int[] getLevelsClearedArray(){ return levelsCleared; }

    public void setLevelsClearedArray(int[] i){
        levelsCleared = i;
    }

    public int getLevelsCleared(int i){
        return levelsCleared[i];
    }

    public void setLevelsCleared(int i, int j) { levelsCleared[i] = j; }

    public void incrementLevelsCleared(int i){
        levelsCleared[i]++;
    }

    public int getWordListLength() {
        return wordListLength;
    }

    public void setWordListLength(int wordListLength) {
        this.wordListLength = wordListLength;
    }

    public String getWordListFile() {
        return wordListFile;
    }

    public void setWordListFile(String wordListFile) {
        this.wordListFile = wordListFile;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    @Override
    public void reset() {

    }
}
